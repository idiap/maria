summary_numeric = function(x){
  list('type'= 'numeric', 'n' = length(x), 'na' = sum(is.na(x)), 'mean' = mean(x,  na.rm=TRUE), 'sd' = sd(x,  na.rm=TRUE))
}
sum_imp_numeric = function(x){
  res = list()
  res[['type']] = 'numeric'
  res[['n']] = mean(laply(x, function(y){y[['n']]}))
  res[['na']] = max(laply(x, function(y){y[['na']]}))
  res[['mean']] = mean(laply(x, function(y){y[['mean']]}))
  res[['sd']] = sqrt(sum(laply(x, function(y){y[['sd']]^2}))/length(x))
  res
}
summary_factor = function(x, base = NULL){
  res = list('type'= 'factor', 'n' = length(x), 'na' = sum(is.na(x)), 'tab' = table(x))
  if( ('0' %in% names(res[['tab']]) | '1' %in% names(res[['tab']])) & length(res[['tab']]) == 1){
    x = factor(x, levels=c('0','1'))
    res = list('type'= 'factor', 'n' = length(x), 'na' = sum(is.na(x)), 'tab' = table(x))
  }
  if(is.null(base)){
    if('1' %in% names(res[['tab']])){
      res[['base']] = '1'
    }else{
      res[['base']] = names(res[['tab']])[1]
    }
  }else{
    res[['base']] = base
  }
  res
}
sum_imp_factor = function(x){
  res = list()
  res[['type']] = 'factor'
  res[['n']] = mean(laply(x, function(y){y[['n']]}))
  res[['na']] = max(laply(x, function(y){y[['na']]}))
  levels = Reduce(union, llply(x, function(y){names(y[['tab']])}))
  tab.imp = llply(x, function(y){
    tab = sapply(levels, function(l){
      if(l %in% names(y[['tab']])){
        y[['tab']][l]
      }else{
        0
      }
    })
    names(tab) = levels
    tab
  })
  res[['tab']] = apply(ldply(tab.imp),2, mean)
  res[['base']] = x[[1]][['base']]
  res
}
summaries = function(d, v_numeric, v_factor, v_factor_specific){
  vars = sort(c(v_numeric, v_factor, names(v_factor_specific)))
  sms = list()
  for(v in vars){
    if(v %in% v_numeric){
      sms[[v]] = summary_numeric(d[,v])
    }
    if(v %in% v_factor){
      sms[[v]] = summary_factor(d[,v])
    }
    if(v %in% names(v_factor_specific)){
      sms[[v]] = summary_factor(d[,v], v_factor_specific[[v]][['base']])
    }
  }
  sms
}
merge_summaries = function(dl){
  final = list()
  for(v in names(dl[[1]])){
    x_imp = llply(dl, function(d){d[[v]]})
    if(dl[[1]][[v]][['type']] == 'numeric'){
      final[[v]] = sum_imp_numeric(x_imp)
    }
    if(dl[[1]][[v]][['type']] == 'factor'){
      final[[v]] = sum_imp_factor(x_imp)
    }
  }
  final
}

print_summaries = function(sms){
  col = sprintf("%s", sms[[1]]$n)
  col.nms = 'n'
  for(v in names(sms)){
    col = c(col, '')
    col.nms = c(col.nms, v)
    i = length(col)
    if(sms[[v]]$type == 'numeric'){
      if( sms[[v]]$na != 0 ){
        col[i] = sprintf(" na:%5.2f%% %5.2f (%6.2f)", sms[[v]]$na / sms[[v]]$n*100, sms[[v]]$mean, sms[[v]]$sd )
      }else{
        col[i] = sprintf("             %5.2f (%6.2f)", sms[[v]]$mean, sms[[v]]$sd )
      }
    }
    if(sms[[v]]$type == 'factor'){
      if(length(sms[[v]]$tab) <= 2){
        if(sms[[v]]$base != '1' | length(sms[[v]]$tab) == 1){
          col.nms[i] = sprintf("%s[%s]", v, sms[[v]]$base)
        }
        m = sms[[v]]$tab[sms[[v]]$base]
        if( sms[[v]]$na != 0 ){
          col[i] = sprintf( "na:%5.2f%%  %d (%5.2f%%)", sms[[v]]$na/ sms[[v]]$n*100, as.integer(m), 100*m / (sms[[v]]$n - sms[[v]]$na) )
        }else{
          col[i] = sprintf( "            %d (%5.2f%%)", as.integer(m), 100*m / sms[[v]]$n )
        }
      }
      if(length(sms[[v]]$tab) > 2){
        col = col[1:(i-1)]
        col.nms = col.nms[1:(i-1)]
        for(l in names(sms[[v]]$tab)){
          col = c(col, sprintf( "            %d (%5.2f%%)", as.integer(sms[[v]]$tab[l]), 100*sms[[v]]$tab[l] / sms[[v]]$n ))
          col.nms = c(col.nms, sprintf("%s[%s]", v, l))
        }
      }
    }
  }
  names(col) = col.nms
  col
}

print_summaries_interval = function(sms, alpha=0.05){
  z = -qnorm(alpha/2)
  col = sprintf("%s", sms[[1]]$n)
  col.nms = 'n'
  for(v in names(sms)){
    col = c(col, '')
    col.nms = c(col.nms, v)
    i = length(col)
    if(sms[[v]]$type == 'numeric'){
      if( sms[[v]]$na != 0 ){
        lo = sms[[v]]$mean - z *  sms[[v]]$sd / sqrt(sms[[v]]$n - sms[[v]]$na)
        hi = sms[[v]]$mean + z *  sms[[v]]$sd / sqrt(sms[[v]]$n - sms[[v]]$na)
        col[i] = sprintf(" na:%5.2f%% %5.2f (%6.2f, %6.2f)", sms[[v]]$na / sms[[v]]$n*100, sms[[v]]$mean, lo, hi )
      }else{
        lo = sms[[v]]$mean - z *  sms[[v]]$sd / sqrt(sms[[v]]$n)
        hi = sms[[v]]$mean + z *  sms[[v]]$sd / sqrt(sms[[v]]$n)
        col[i] = sprintf("             %5.2f (%6.2f, %6.2f)", sms[[v]]$mean, sms[[v]]$sd, lo, hi )
      }
    }
    if(sms[[v]]$type == 'factor'){
      if(length(sms[[v]]$tab) <= 2){
        if(sms[[v]]$base != '1' | length(sms[[v]]$tab) == 1){
          col.nms[i] = sprintf("%s[%s]", v, sms[[v]]$base)
        }
        m = sms[[v]]$tab[sms[[v]]$base]
        if( sms[[v]]$na != 0 ){
          p = m / (sms[[v]]$n - sms[[v]]$na)
          lo = p - z *  sqrt(p * (1-p)) / sqrt(sms[[v]]$n - sms[[v]]$na)
          hi = p + z *  sqrt(p * (1-p)) / sqrt(sms[[v]]$n - sms[[v]]$na)
          col[i] = sprintf( "na:%5.2f%%  %5.2f%% (%5.2f%%, %5.2f%%)", sms[[v]]$na/ sms[[v]]$n*100, 100*p, 100*lo, 100*hi)
        }else{
          p = m / (sms[[v]]$n)
          lo = p - z *  sqrt(p * (1-p)) / sqrt(sms[[v]]$n )
          hi = p + z *  sqrt(p * (1-p)) / sqrt(sms[[v]]$n)
          col[i] = sprintf( "            %5.2f%% (%5.2f%%, %5.2f%%)", 100*p, 100*lo, 100*hi)
        }
      }
      if(length(sms[[v]]$tab) > 2){
        col = col[1:(i-1)]
        col.nms = col.nms[1:(i-1)]
        for(l in names(sms[[v]]$tab)){
          p = sms[[v]]$tab[l] / sms[[v]]$n
          lo = p - z *  sqrt(p * (1-p)) / sqrt(sms[[v]]$n )
          hi = p + z *  sqrt(p * (1-p)) / sqrt(sms[[v]]$n)
          col = c(col, sprintf( "            %5.2f%% (%5.2f%%, %5.2f%%)", 100*p, 100*lo, 100*hi))
          col.nms = c(col.nms, sprintf("%s[%s]", v, l))
        }
      }
    }
  }
  names(col) = col.nms
  col
}
standardized_differences = function(A, B){
  ## http://support.sas.com/resources/papers/proceedings12/335-2012.pdf
  d = rep(0, length(A))
  names(d) = names(A)
  for(v in names(A)){
    if(A[[v]]$type == 'numeric'){
      d[v] = (A[[v]]$mean - B[[v]]$mean) / sqrt( (A[[v]]$sd^2 + B[[v]]$sd^2) / 2 )
    }
    if(A[[v]]$type == 'factor'){
      if(length(A[[v]]$tab) > 2){
        pa = A[[v]]$tab / (A[[v]]$n - A[[v]]$na)
        pb = B[[v]]$tab / (B[[v]]$n - B[[v]]$na)
        d.sub = (pa-pb) / sqrt( (pa * ( 1-pa) + pb * ( 1-pb))/2 )
        names(d.sub) = sprintf("%s[%s]", v, names(A[[v]]$tab))
        i = match(v, names(d))
        d = c(d[1:(i-1)], d.sub, d[(i+1):length(d)])
      }else{
        pa = A[[v]]$tab[A[[v]]$base] / (A[[v]]$n - A[[v]]$na)
        pb = B[[v]]$tab[B[[v]]$base] / (B[[v]]$n - B[[v]]$na)
        d[v] = (pa-pb) / sqrt( (pa * ( 1-pa) + pb * ( 1-pb))/2 )
      }
    }
  }
  d
}