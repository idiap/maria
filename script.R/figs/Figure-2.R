source('/home/idiap/projects/maria/script.R/util_initialization.R')

##
## IMPORTANT:
##
## És necessari l'input models que conté els models ajustats
##

eps = c('ep_all_death', 'ep_comb.all_hard_ISQ')
cc = list()
for(ep in eps){
  cc[[ep]] = survival_curve(event="event", time="T0", time2="T1", group="timevar",
                            D=llply(models, function(mm){mm$data[[ep]]}),
                            km.curve=F)
}

## Es defineixen les dades per generar el gràfic
titles = c('Total Mortality', 'Major Cardiovascular Event')
names(titles) = eps
df = data.frame(time = c(), s = c(), ch = c(), user = c(), ep = c())
for(e in eps){
  D = cc[[e]]
  df.temp = D[,c('time', 's0', 'ch0')]
  names(df.temp) = c('time', 's', 'ch')
  df.temp$user = 0
  df.temp$ep = titles[e]
  df = rbind(df, df.temp)
  df.temp = D[,c('time', 's1', 'ch1')]
  names(df.temp) = c('time', 's', 'ch')
  df.temp$user = 1
  df.temp$ep = titles[e]
  df = rbind(df, df.temp)
}
df$user = as.factor(df$user)

### Gràfic GGPLOT2
p <- ggplot(df, aes(x=time, y=ch, linetype=user)) + facet_grid(. ~ ep) + 
  geom_line()+ 
  xlab('Time (years)') + ylab('cummulative hazard')+
  scale_linetype(name=NULL, labels=c("Statin Non-User", "Statine User"))+
  scale_x_continuous(breaks=seq(0,5))+
  theme_bw()+theme(strip.background = element_rect(fill='white', colour="white"),
                   strip.text.x = element_text(size=12, family="sans", face='bold'),
                   panel.border = element_rect(colour="white"),
                   legend.position="bottom",
                   legend.key=element_blank(),
                   legend.title=element_blank())
print(p)
if(FALSE){
  tiff(file='doc/Figure-2.tiff', width = 1000, height = 480, compression = 'none')
  print(p)
  dev.off()
}
