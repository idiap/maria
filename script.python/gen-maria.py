# -*- coding: utf-8 -*-

##
## Canvis a 22-01-2014:   
## stat_years = 1.5 -->  stat_years = 3
## s'agafa la primera mesura d'itb --> sorted(list(itb[i]))

from nova2.population import Population
from nova2.problems import Problems
from nova2.treatment import Treatment
from nova2.cohort import Cohort
from nova2.variable import Variable
from nova2.visits import Visits
from nova2.eq import EQ
from nova2.paths import Paths
from utils import misc
import datetime

# Study period
begin_study = datetime.date(2006,4,1)
end_study   = datetime.date(2011,12,31)

# Cohort intro period
intro_from = datetime.date(2006,4,1)
intro_until = datetime.date(2011,12,31)

# Population age
min_age = 35
max_age = 84.999

# Exclusion 
l_antecedents = ['angor', 'ihd_acute', 'ihd_chronic', 'ami_atc', 'stroke_i', 'stroke_e', 'stroke_h', 'tia']
l_procedures = ['card_proc', 'peri_proc']

# End-points avoided M months after statine beginning
M = 1
hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia']
stat_years = 1.5 # statine beginning allowed after itb measure
mpr_months = 6 # months for mpr calculation

# Months without statine for statine beginning and ending
stat_months_beg = 6
stat_months_end = 6

# Problems at dintro
probs_list = ['smoking', 'ard', 'obesity', 'diabetes', 'alcoholism', 'htn', 'aff', 
         'lupus', 'dyslipidemia', 'arthritis', 'asthma', 'copd', 'hepatopathy', 
         'miopathy', 'hypotirodism', 'depression', 'cancer', 'pad', 
         'neoplasms_malignant', 'neoplasms_benign', 'diabetes_2']

# Variables at dintro
vnames = ['alb', 'alcohol', 'basofils_p', 'cac', 'colhdl', 'colldl', 'coltot', 'cre', 
          'eosinofils_p', 'fa', 'ggt', 'glu', 'got', 'gpt', 'hba1c', 'imc', 'leucocits', 
          'limfocits_p', 'mdrd', 'monocits_p', 'neutrofils_p', 'pes', 'tabac', 'tad', 
          'talla', 'tas', 'tg', 'itb']

# Facturation at dintro (variable indicates if a facturation of given med exists during
# last i months 
meds_persistence = {'aspirine': 3, 'c10-no-statine': 6, 'g03a': 3,  'c02': 3, 'c03': 3, 
                    'c07': 3, 'c08': 3, 'c09': 3, 'a10': 3, 'n06': 6, 'm01': 3, 'n05': 6, 
                    'h02': 3 }

##### Incidence
# Problem incidence
incidence_probs = hard_ep + ['stroke_h', 'hf', 'sudden_death_card', 'adverse_effects', 
                             'miopathy', 'ard', 'hepatopathy', 'neoplasms_malignant', 
                             'neoplasms_benign', 'diabetes_2', 'ami_atc',
                             'cataract', 'diabetes', 'ihd_acute', 'ihd_chronic', 'cancer', 
                             'pad', 'stroke_ie', 'stroke_i2', 'stroke_i3', 'stroke_i4', 
                             'stroke_i5', 'stroke_h2', 'stroke_h3', 'tia_219', 'ami_219']
                             
# Procedure incidence
incidence_procs = ['card_proc', 'peri_proc', 'stroke_proc']

paths = Paths('2012')

# Initial population
population = Population(paths.get('sidiap'))

itb = Variable(population = population, filename = paths.variable('itb')).allMeasures(population, as_dict=True)
itb_first2 = dict()
for ocip in list(itb):
    if ocip not in itb_first2:
        cont = True
        for d in itb[ocip]:
            for m_itb in itb[ocip][d]:
                meas = float(m_itb)
                if 0.3999 <= meas and meas <= 0.9501 and cont:
                    itb_first2[ocip] = d
                    cont = False
                    break

itb_first = dict()
for l in [ (i,d,m) for i in list(itb) for d in sorted(list(itb[i])) for m in range(len(itb[i][d])) if 0.40 <= float(itb[i][d][m]) and float(itb[i][d][m]) <= 0.9501 ]:
    if l[0] not in itb_first:
        itb_first[l[0]] = l[1]

population = Population(filename = paths.get('sidiap'), filtering = itb_first, cmbd_path=paths.get('cmbd'))

# Problems creation using population
problems = Problems(paths)

# Defining the cohort
cohort = Cohort(population = population, 
                beginning = begin_study, 
                ending = end_study, 
                minIntro = intro_from, 
                maxIntro = intro_until)

cohort.setStatusFilter(population)    # Alive, death or tranferred filter
ev = problems.get_events(cohort.cohort, l_antecedents)
cohort.setMaxIntroDate( problems.get_absolute_minimum(ev) ) # Cardio-vascular filter
pr = problems.get_procedures(cohort.cohort, l_procedures)
cohort.setMaxIntroDate( problems.get_absolute_minimum(pr) ) # procedure filter

cohort.setAgeFilter(population, minAge = min_age, maxAge = max_age)

cohort.setMinIntroDate(itb_first)

### Eliminating old users
statines = Treatment(population = cohort.cohort, filename = paths.treatment('statine'), catalog = paths.get('catalog') )

old_statine_users = statines.lastBeginning(cohort.cohort, months = stat_months_beg, before = cohort._minIntro)

statines.filtering(old_statine_users)
old_statine_users_end = statines.firstEnding(old_statine_users, months = stat_months_end, after = old_statine_users)

cohort.setMinIntroDate(old_statine_users_end)
#n=1022582

# End-points avoided three months after statine beginning
first_hard_event = problems.get_absolute_minimum( problems.get_events(cohort.cohort, hard_ep) )

stat_in_prev_to = dict()
for ocip in cohort._maxIntro:
    if ocip in first_hard_event:
        stat_in_prev_to[ocip] = min(cohort._maxIntro[ocip], first_hard_event[ocip] - datetime.timedelta(M*30) )
    else:
        stat_in_prev_to[ocip] = cohort._maxIntro[ocip]

statines = Treatment(population = cohort.cohort, filename = paths.treatment('statine'), catalog = paths.get('catalog') )
beginning = statines.firstBeginning(cohort.cohort, months = stat_months_beg, after = cohort._minIntro, before = stat_in_prev_to)
mpr = statines.mpr(beginning, dfrom=beginning, duntil=misc.addToDate(beginning, datetime.timedelta(6*30)))
first_statine = dict()
for ocip in beginning:
    first_statine[ocip] = beginning[ocip]

# Ningú pot entrar després d'iniciar tractament d'estatines
cohort.setMaxIntroDate(beginning)


# Defining allowed period statine intro (3 years)
end_period = dict()
for ocip in cohort.cohort:
    end_period[ocip] = min(cohort._maxIntro[ocip], cohort._minIntro[ocip] + datetime.timedelta(365*stat_years))

cohort.setMaxIntroDate(end_period)

beginning = statines.firstBeginning(cohort.cohort, months = stat_months_beg, after = cohort._minIntro, before = stat_in_prev_to, getAll=True)

statine_intro = dict()
statine_info = dict()
for ocip in beginning:
    statine_intro[ocip] = beginning[ocip][0]
    statine_info[ocip]  = beginning[ocip][1:3]

statine_ending = statines.firstEnding(cohort.cohort, months = stat_months_end, after = cohort._minIntro)

cohort.setIntro()


cohort.addData(cohort.cohortIntro, 'dintro')
cohort.addData(first_statine, 'first.statine')
cohort.addData(statine_intro, 'statine.beginning')
cohort.addData(statine_ending, 'statine.ending')
cohort.addData(statine_info, ['statine.atc', 'statine.env'])

cohort.addData(population.ageAt(cohort.cohortIntro), 'age')
cohort.addData(population.sex(cohort.cohort), 'sex')

cohort.addData(misc.getValue(paths.get('medea')), 'medea')
cohort.addData(misc.getValue(paths.get('medea'), ivalue=2), 'rural')

eq = EQ(population, eqafile = paths.get('eqa'), eqffile = paths.get('eqf'), relfile = paths.get('rel'))
cohort.addData(eq.getEQs(cohort.cohortIntro), ['UBA', 'EQA', 'EQF'])

cohort.addBinaryData(statine_intro, 'user')

cohort.addData(cohort._exitus_reason, 'exitus')
cohort.addData(cohort._exitus, 'dexitus')

cohort.addData(mpr, 'mpr')

visits = Visits(paths.get('visits'))
cohort.addData( visits.numbervisits(cohort.cohortIntro) , 'visits', default_na = 0)
cohort.addData( visits.visit_previous_to(statine_intro) , 'prev_visit')

problems.filtering(cohort.cohort)

evts = problems.get_events(cohort.cohort, probs_list, before=cohort.cohortIntro)
cohort.addBinaryData(evts, probs_list)

# Es calcula la data a partir de la qual no es pot agafar la mesura de colesterol. 
# Ha de ser dins del període d'inclusió, i a tres mesos de l'inicia d'estatina
col_measure_date = dict()
for ocip in cohort._maxIntro:
    if ocip in statine_intro:
        col_measure_date[ocip] = cohort._maxIntro[ocip] + datetime.timedelta(-3*30)
    else:
        col_measure_date[ocip] = cohort._maxIntro[ocip]

colesterols = ['coltot', 'colldl', 'colhdl', 'tg']
tot = Variable(population = cohort.cohort, filename = paths.variable('coltot')).allMeasures(cohort.cohort, before = col_measure_date, as_dict=True)
hdl = Variable(population = cohort.cohort, filename = paths.variable('colhdl')).allMeasures(cohort.cohort, before = col_measure_date, as_dict=True)
ldl = Variable(population = cohort.cohort, filename = paths.variable('colldl')).allMeasures(cohort.cohort, before = col_measure_date, as_dict=True)
tg = Variable(population = cohort.cohort, filename = paths.variable('tg')).allMeasures(cohort.cohort, before = col_measure_date, as_dict=True)

# Sols es consideren mesures en un mateix dia
cohort.addData( misc.nearest(cohort.cohortIntro, misc.mergeMeasurements(tot, hdl, ldl, tg)), ['coltot', 'colhdl', 'colldl', 'tg'])

tensions = ['tas', 'tad']
tas = Variable(population = cohort.cohort, filename = paths.variable('tas')).allMeasures(cohort.cohort, before = cohort._maxIntro, as_dict=True)
tad = Variable(population = cohort.cohort, filename = paths.variable('tad')).allMeasures(cohort.cohort, before = cohort._maxIntro, as_dict=True)

cohort.addData( misc.nearest(cohort.cohortIntro, misc.mergeMeasurements(tas, tad)), ['tas', 'tad'])

itb = Variable(population = cohort.cohort, filename = paths.variable('itb')).allMeasures(cohort.cohort, before = cohort._maxIntro, as_dict=True)
itb_m = misc.nearest(itb_first, itb)

cohort.addData( { ocip: m for ocip in itb_m for m in itb_m[ocip] if  0.40 <= float(m) and float(m) <= 0.9501}, 'itb')


variables = list(set(vnames) - set(colesterols) - set(tensions) - set(['itb']))

for vname in variables:
    var = Variable(population = cohort.cohort, filename = paths.variable(vname))
    cohort.addData(var.lastMeasure(cohort.cohort, before=cohort.cohortIntro), ['d' + vname, vname])

for m in meds_persistence:
    treatment = Treatment(population = cohort.cohort, filename = paths.treatment(m), catalog = paths.get('catalog') )
    effect = datetime.timedelta(30 * meds_persistence[m])
    cohort.addData(treatment.statusAt(cohort.cohortIntro, effect = effect), m, default_na = '0')

## EVENT INCIDENCE
evts = problems.get_events(cohort.cohort, incidence_probs, after = cohort.cohortIntro, before = cohort._exitus)
cohort.addData(problems.get_relative(evts, incidence_probs, which=0), ['ep_' + i for i in incidence_probs], default_na = 'NA')

## PROCEDURES INCIDENCE
procs = problems.get_procedures(cohort.cohort, incidence_procs, after = cohort.cohortIntro, before = cohort._exitus)
cohort.addData(problems.get_relative(procs, incidence_procs, which=0), ['ep_' + i for i in incidence_procs], default_na = 'NA')

#hard_incidence = hard_ep + ['pad']

### ONLY CMBD INCIDENCE
#probs = problems.first_event(hard_incidence, use_ecap = False, after = cohort.cohortIntro, before = cohort._exitus, only_first = True, absolute = False)
#cohort.addData(probs, ['ep_' + i + '.cmbd' for i in hard_incidence])

### ONLY ECAP INCIDENCE
#probs = problems.first_event(hard_incidence, use_cmbd = False, after = cohort.cohortIntro, before = cohort._exitus, only_first = True, absolute = False)
#cohort.addData(probs, ['ep_' + i +'.ecap' for i in hard_incidence])

incidence_deaths = ['coronari_i_death', 'stroke_i_death', 'cv_death', 'cvrestrict_death', 
                    'nd_death', 'other_death', 'pad_i_death', 'stroke_i_death2', 
                    'coronari_i_death2', 'CV', 'all_death', 'cardio_death', 'cancer_death',
                    'no_cancer_death']
deaths = problems.death(cohort.cohort, incidence_deaths, after = cohort.cohortIntro)
cohort.addData(deaths, ['ep_' + i for i in incidence_deaths])

cohort.writeTable("maria.csv")

